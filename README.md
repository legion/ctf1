# Level 1
Welcome to Legion's first CTF challenge. Your goal is to figure out the value for the key ctfcode in a namespace that is not known to you.
# Running

- Install node and npm (see http://nodejs.org/)
- Run `npm install` from this directory to install dependencies
- Run `node level00.js` to start the server on port 3000
- Go to [http://localhost:3000](http://localhost:3000) in your browser
